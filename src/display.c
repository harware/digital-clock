#include "display.h"

#include "hascii_constants.h"
#include "max.h"
#include "state.h"
#include <avr/interrupt.h>
#include <avr/io.h>

// NOTE: HASCII representation
// Byte values that produce our custom character set (the digits 0 to 9 and
// characters 'o', 'n' and '-') in the seven-segment display. The seven segment
// displays are driven in such a way that the most significant bit of the
// output of these characters goes to the decimal point, and the bits 6 to 0
// go to segments from a to g.
const static uint8_t _characters[] = {0x7E, 0x30, 0x6D, 0x79, 0x33, 0x5B,
                                      0x5F, 0x70, 0x7F, 0x7B, 0x77, 0x04,
                                      0x0E, 0x15, 0x1D, 0x0F, 0x01, 0x4F};

// brightness for the display
static uint8_t brightness = HALF_INTENSITY;

// Pointer to display memory
static uint8_t *_displayCharacters;

// Callback to get display Hascii indices
static void (*_getDisplayHascii)(uint8_t **values);
// Callback to check whether display update is necessary
static bool (*_isDisplayStale)();
// Value to store whether the blinking phase is on or off currently
static volatile bool _blinkPhaseLow = false;
// Whether the blink phase has changed recently
static volatile bool _blinkPhaseChanged = true;

static void _initBlinkTimer() {
    // Default (0) values for all configuration bits in control register A
    TCCR2A = 0x00;
    // Set maximum prescaling (/1024) by setting bits CS22, CS21 and CS20
    TCCR2B = 0x07;
    // Enable overflow interrupt
    TIMSK2 = (1 << TOIE2);
    // Reset the count
    TCNT2 = 0;
}

ISR(TIMER2_OVF_vect) { // NOLINT (clang-diagnostic-unknown-attribute)
    _blinkPhaseLow = !_blinkPhaseLow;
    _blinkPhaseChanged = true;
}

// initialize display module
void display_init() {
    max_init();
    _initBlinkTimer();
}

// Send the display data in the given pointer to display driver
static void _sendDisplayData(uint8_t *dataBytes) {
    uint8_t blinkFlags = dataBytes[DIGITS_COUNT + 1];
    for (uint8_t i = 0; i < DIGITS_COUNT; i++) {
        // Get the decoded value (segments) that we need to display
        uint8_t data =
            _characters[(dataBytes[i] > DISPLAY_UNKNOWN) ? DISPLAY_UNKNOWN
                                                         : dataBytes[i]];
        if (_blinkPhaseLow && bit_is_set(blinkFlags, i)) {
            data = 0x00;
        }
        // Addresss is i + 1 because display address begins from 1 for MAX7219
        max_writeAddressAndData(i + 1, data);
    }
    // The second-to last byte is the main mode indication, and we use
    // LED_COUNT + 1 as the address because it is higher than the seven
    // segments
    max_writeAddressAndData(DIGITS_COUNT + 1,
                            1 << (6 - _displayCharacters[DIGITS_COUNT]));
}

// display characters is in HASCII
void display_update() {
    // Sit idle for a while to prevent too frequent polling of display
    // freshness
    _delay_ms(4);
    // Nothing can be done if our callbacks are not set and nothing will be
    // done if the display is still fresh
    if (!_isDisplayStale || !_getDisplayHascii ||
        (!_isDisplayStale() && !_blinkPhaseChanged)) {
        return;
    }
    _blinkPhaseChanged = false;
    (*_getDisplayHascii)(&_displayCharacters);
    _sendDisplayData(_displayCharacters);
}

// set callback to query whether display needs to be updated
void display_setIsDisplayStaleCallback(bool (*isDisplayStale)()) {
    _isDisplayStale = isDisplayStale;
}

// change brightness for the seven segment display with provided steps
void display_changeBrightness(int8_t steps) {
    brightness = min(15, max(1, brightness + steps));
    max_writeAddressAndData(INTENSITY_REGISTER, brightness);
}

// set callback to get set of display Hascii
void display_setDisplayHasciiCallback(
    void (*setDisplayHascii)(uint8_t **values)) {
    _getDisplayHascii = setDisplayHascii;
}
