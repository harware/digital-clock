#include "rtc.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdbool.h>
#include <util/twi.h>

#include "common.h"

#define BUS_PINS_MASK 0x30
// The SLA_W and SLA_R bytes are composed of the SLave Address and the
// W/R bit. The slave address for the DS1307 is 1101000. The LSB should
// be 0 for writes and 1 for reads.
#define SLA_W 0xD0
#define SLA_R 0xD1

#define TWCR_START 0xA5
#define TWCR_DATATX 0x85
#define TWCR_DATARX_ACK 0xC5
#define TWCR_DATARX_NACK 0x85
#define TWCR_STOP 0x95

// We define a convenience macro as the bitwise and of the TWI status register
// TWSR and the mask 0xF8 that zeros out the 3 least significant bits. The 3
// least significant bits are not status bit but clock pre-scale values for the
// bus clock.
#define STATUS (TWSR & 0xF8)

enum TWIState {
    IDLE,
    WRITE_STARTED,
    WRITE_ADDRESSED,
    WRITE_REGPNTR_SET,
    WRITE_WRITING,
    READ_STARTED,
    READ_ADDRESSED,
    READ_REGPNTR_SET,
    READ_RE_STARTED,
    READ_RE_ADDRESSED,
    READ_READING
};

static volatile enum TWIState _state = IDLE;
static struct DataPacket _dataPacket;
static volatile uint8_t _nextByte = 0;
static void (*_onWriteDone)();
static void (*_onReadDone)();

/**
 * Write 'length' number of bytes from 'bytes' to the DS1307's battery-backed
 * RAM, starting at 'address'.
 */
bool rtc_putBytes(struct DataPacket *dataPacket, void (*onWriteDone)()) {
    if (_state != IDLE) {
        return false;
    }

    _onWriteDone = onWriteDone;
    _dataPacket = *dataPacket;
    _state = WRITE_STARTED;
    TWCR = TWCR_START;
    return true;
}

/**
 * Read 'length' number of bytes to 'bytes' from the DS1307's battery-backed
 * RAM, starting at 'address'.
 */
bool rtc_getBytes(struct DataPacket *dataPacket, void (*onReadDone)()) {
    if (_state != IDLE) {
        return false;
    }

    _onReadDone = onReadDone;
    _dataPacket = *dataPacket;
    _state = READ_STARTED;
    TWCR = TWCR_START;
    return true;
}

static void _handleReadInterrupt() {
    if (_state == READ_STARTED && STATUS == TW_START) {
        // A START signal has been actually transmitted
        // Write the slave address and request a write
        TWDR = SLA_W;
        // Set the control register to send a data byte
        TWCR = TWCR_DATATX;
        // We have finished addressing the RTC for an read transaction
        _state = READ_ADDRESSED;
    } else if (_state == READ_ADDRESSED && STATUS == TW_MT_SLA_ACK) {
        // The slave has acknowledged our SLA transmission. Now we write
        // an address byte to set the DS1307's register pointer.
        TWDR = _dataPacket.address;
        // Set the control register to send a data byte
        TWCR = TWCR_DATATX;
        // We have finished setting the register pointer for the read txn
        _state = READ_REGPNTR_SET;
    } else if (_state == READ_REGPNTR_SET && STATUS == TW_MT_DATA_ACK) {
        // The slave has acknowledged our register pointer transmission.
        // Set the control register to send a (repeated) start signal
        TWCR = TWCR_START;
        // START has been done for the actual read part
        _state = READ_RE_STARTED;
    } else if (_state == READ_RE_STARTED && STATUS == TW_REP_START) {
        // A repeated START signal has been sent
        // Send the slave address and request a read
        TWDR = SLA_R;
        // Set the control register to transmit data (for SLA_R)
        TWCR = TWCR_DATATX;
        // The device has been re-addressed for the read transaction
        _state = READ_RE_ADDRESSED;
    } else if (_state == READ_RE_ADDRESSED && STATUS == TW_MR_SLA_ACK) {
        // The slave address has been acknowledged.
        // Set the TWCR to either acknowledge or non-acknowledge the byte that
        // will be received, according to how many bytes we need to read.
        TWCR = _dataPacket.length == 1 ? TWCR_DATARX_NACK : TWCR_DATARX_ACK;
        // We are now ready to read
        _state = READ_READING;
        _nextByte = 0;
    } else if (_state == READ_READING) {
        if ((STATUS == TW_MR_DATA_ACK) &&
            (_nextByte < (_dataPacket.length - 1))) {
            // Write the read data byte
            _dataPacket.bytes[_nextByte] = TWDR;
            // Set the TWI control register to acknowledge or non-acknowledge
            // the next byte according to whether we are at the second-to-last
            // byte or not.
            TWCR = (_nextByte == _dataPacket.length - 2) ? TWCR_DATARX_NACK
                                                         : TWCR_DATARX_ACK;
        }
        if ((STATUS == TW_MR_DATA_NACK) &&
            (_nextByte == (_dataPacket.length - 1))) {
            _dataPacket.bytes[_nextByte] = TWDR;
            // Set the control register to stop the transaction
            TWCR = TWCR_STOP;
            // Reset the byte counter and the state
            _nextByte = 0;
            _state = IDLE;
            // Finally, call the  callback because we have all the bytes we
            // want
            if (_onReadDone) {
                _onReadDone();
            }
        } else {
            // If it wasn't the last byte, we need to increment this counter
            _nextByte++;
        }
    }
}

static void _handleWriteInterrupt() {
    if (_state == WRITE_STARTED && STATUS == TW_START) {
        // A START signal has been actually transmitted
        // Write the slave address and request a write
        TWDR = SLA_W;
        // Set the control register to send a data byte
        TWCR = TWCR_DATATX;
        // We have finished addressing the RTC for a write transaction
        _state = WRITE_ADDRESSED;
    } else if (_state == WRITE_ADDRESSED && STATUS == TW_MT_SLA_ACK) {
        // The slave has acknowledge our SLA transmission. Now we write
        // an address byte to set the DS1307's register pointer.
        TWDR = _dataPacket.address;
        // Set the control register to send a data byte (the address)
        TWCR = TWCR_DATATX;
        // We have finished setting the register pointer for the write txn
        _state = WRITE_REGPNTR_SET;
        // Reset the byte counter
        _nextByte = 0;
    } else if ((_state == WRITE_REGPNTR_SET || _state == WRITE_WRITING) &&
               STATUS == TW_MT_DATA_ACK) {
        if (_nextByte == _dataPacket.length) {
            // Writing is over
            _state = IDLE;
            // Reset byte counter
            _nextByte++;
            // Send stop signal
            TWCR = TWCR_STOP;
            // And call the callback
            if (_onWriteDone) {
                _onWriteDone();
            }
        } else {
            // Change state
            _state = WRITE_WRITING;
            // Write data to the data register and increment byte counter
            TWDR = _dataPacket.bytes[_nextByte];
            _nextByte++;
            // Set the control register to transmit data
            TWCR = TWCR_DATATX;
        }
    }
}

ISR(TWI_vect) { // NOLINT (clang-diagnostic-unknown-attributes)
    if (_state == IDLE) {
        return;
    } else if (_state >= READ_STARTED) {
        _handleReadInterrupt();
    } else {
        _handleWriteInterrupt();
    }
}

/**
 * Initialize the Real Time Clock
 */
void rtc_init() {
    // Initialize state
    _state = IDLE;
    // The SDA and SCL are also PC4 and PC5. As we need the TWI bus lines to
    // be pulled-up at all times, we set the pins to input and enable the
    // pull-up resistors.
    DDRC &= ~BUS_PINS_MASK;
    PORTC |= BUS_PINS_MASK;
    // Set the TWI bit rate register to 0, which ensures the smallest possible
    // divisor and thus the highest possible bit rate. As we are running on a
    // CPU clock speed of 1Mhz, even the fastest bit rate will only be 62.5Khz,
    // slower than the low-speed mode of the bus (100Khz) that the RTC module
    // supports.
    TWBR = 0x00;
    TWSR &= ~3;
}

/**
 * Return true if the RTC communication module is busy communicating with
 * the RTC. The rtc_getBytes and rtc_putBytes operations will fail (return
 * false) if they are called when the RTC module is busy.
 */
bool rtc_isBusy() { return _state != IDLE; }
