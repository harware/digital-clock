#include "state_beep.h"

#include "display.h"
#include "hascii_constants.h"
#include <avr/interrupt.h>
#include <avr/io.h>

#define BEEP_DDR DDRB
#define BEEP_PORT PORTB
#define BEEP_PIN PB6

static void _onEnterState();
static void _onChangePress();
static void _onChangeLongPress();
static void _onRotaryKnobTurn(int8_t steps);
static void _onQueryHascii(uint8_t **values);
static bool _onQueryDisplayStale();

static void (*beepStopCallback)();
static uint8_t _song;
static uint8_t _timeout;
static uint8_t *_beepDisplay;
static volatile uint8_t _beeperTicks = 0;
static bool _beepStateDirty = false;

struct BeepState beepState = {
    .isBeeping = false,
    .handlers =
        {
            .onEnterState = _onEnterState,
            .onChangeLongPress = _onChangeLongPress,
            .onChangePress = _onChangePress,
            .onQueryHascii = _onQueryHascii,
            .onQueryDisplayStale = _onQueryDisplayStale,
            .onRotaryKnobTurn = _onRotaryKnobTurn,
        },
};

ISR(TIMER0_OVF_vect) { // NOLINT (clang-diagnostic-unknown-attribute)
    if (beepState.isBeeping) {
        _beeperTicks = (_beeperTicks + 1) % 2;
        if (_beeperTicks == 0) {
            BEEP_PORT ^= 1 << BEEP_PIN;
        }
    }
}

static void _startBeepTimer() {
    // Maximum prescaling (/1024)
    TCCR0B = 0x05;
    TCCR0A = 0x00;
    // Enable overflow interrupt
    TIMSK0 = 1 << TOIE0;
    // Clear timer count
    TCNT0 = 0;
}

static void _stopBeepTimer() {
    TCCR0A = 0x00;
    TCCR0B = 0x00;
    TIMSK0 = 0x00;
}

// start the beep state initialized at given time by given mode
// timeout can not be zero
void beep_startBeeping(uint8_t *display, uint8_t timeout, uint8_t song) {
    _song = song;
    _timeout = timeout;
    _beepDisplay = display;
    beepState.isBeeping = true;
    BEEP_PORT |= 1 << BEEP_PIN;
    _startBeepTimer();
}

// stop the beeping and return back to normal
void beep_stopBeeping() {
    beepState.isBeeping = false;
    beepStopCallback();
    BEEP_PORT &= ~(1 << BEEP_PIN);
    _stopBeepTimer();
}

static void _onEnterState() { _beepStateDirty = true; }

// Handle 'Push C' event
// Forcefully stops the beep mode
static void _onChangePress() { beep_stopBeeping(); }

// Handle 'Long Push C' event
// Forcefully stops the beep mode
static void _onChangeLongPress() { beep_stopBeeping(); }

// Handle 'Rotary knob turn' event
static void _onRotaryKnobTurn(int8_t steps) { display_changeBrightness(steps); }

// Handle a display hascii query
static void _onQueryHascii(uint8_t **values) {
    _beepStateDirty = false;
    *values = _beepDisplay;
}

// Handle a display freshness query
static bool _onQueryDisplayStale() { return _beepStateDirty; }

// setup beep mode configuration
void beep_init() {
    // Set data direction for beep pin
    BEEP_DDR |= (1 << BEEP_PIN);
    // Keep beep output low initially
    BEEP_PORT &= ~(1 << BEEP_PIN);
};

// set the callback to call when beep gets timed out or forced closed
void beep_setStopCallback(void *stopCallback) {
    beepStopCallback = stopCallback;
}

// Handle actions to be performed at each time change event
// Generally updates duration and finally stop beeping after a minute
void beep_onTimeChange(struct Time *time) {
    if (beepState.isBeeping) {
        _timeout--;

        if (_timeout == 0) {
            beep_stopBeeping();
        }
    }
}
