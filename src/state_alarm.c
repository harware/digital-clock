#include "state_alarm.h"

#include "display.h"
#include "hascii_constants.h"

static void _onEnterState();
static void _onChangePress();
static void _onChangeLongPress();
static void _onRotaryKnobTurn(int8_t steps);
static void _onQueryHascii(uint8_t **values);
static bool _onQueryDisplayStale();

static uint8_t _song = 0;
static uint8_t _timeout = 30;
static uint8_t _beepDisplay[8];
static uint8_t _alarmDisplay[8];
static bool _alarmStateDirty = true;
static void (*_startBeep)(uint8_t *display, uint8_t timeout, uint8_t song);

struct AlarmState alarmState = {
    .alarmSubMode = ALARM_VIEW,
    .isEnabled = false,
    .tmpIsEnabled = false,
    .time =
        {
            .hour = 6,
            .minute = 0,
        },
    .handlers =
        {
            .onEnterState = _onEnterState,
            .onChangeLongPress = _onChangeLongPress,
            .onChangePress = _onChangePress,
            .onQueryHascii = _onQueryHascii,
            .onQueryDisplayStale = _onQueryDisplayStale,
            .onRotaryKnobTurn = _onRotaryKnobTurn,
        },
};

void alarm_setBeepStartCallback(void (*startBeep)(uint8_t *display,
                                                  uint8_t timeout,
                                                  uint8_t song)) {
    _startBeep = *startBeep;
}

// prepare HASCII list to display while beeping for alarm
static void _prepareBeepingDisplay() {
    _beepDisplay[0] = tensDigit(alarmState.time.hour);
    _beepDisplay[1] = onesDigit(alarmState.time.hour);
    _beepDisplay[2] = tensDigit(alarmState.time.minute);
    _beepDisplay[3] = onesDigit(alarmState.time.minute);
    _beepDisplay[4] = DISPLAY_A;
    _beepDisplay[5] = DISPLAY_L;
}

static inline void _setSubMode(enum AlarmSubMode subMode) {
    alarmState.alarmSubMode = subMode;
    _alarmStateDirty = true;
}

static inline void _updateTmpIsEnabled(bool isEnabled) {
    alarmState.tmpIsEnabled = isEnabled;
    _alarmStateDirty = true;
}

// update alarm state as provided
static inline void _updateAlarm(struct Time *time, bool isEnabled) {
    alarmState.time.hour = time->hour;
    alarmState.time.minute = time->minute;
    alarmState.isEnabled = isEnabled;
    _alarmStateDirty = true;
}

static inline void _updateTmpAlarm(struct Time *time, bool isEnabled) {
    alarmState.tmpTime.hour = time->hour;
    alarmState.tmpTime.minute = time->minute;
    alarmState.tmpIsEnabled = isEnabled;
    _alarmStateDirty = true;
}

static inline void _updateTmpTimeHour(uint8_t hour) {
    alarmState.tmpTime.hour = hour;
    _alarmStateDirty = true;
}

static inline void _updateTmpTimeMinute(uint8_t minute) {
    alarmState.tmpTime.minute = minute;
    _alarmStateDirty = true;
}

static inline void _recomputeDisplay() {
    struct Time *toDisplay = &alarmState.tmpTime;
    bool isAlarmEnabled = alarmState.tmpIsEnabled;

    if (alarmState.alarmSubMode == ALARM_VIEW) {
        toDisplay = &alarmState.time;
        isAlarmEnabled = alarmState.isEnabled;
    }

    _alarmDisplay[0] = isAlarmEnabled ? DISPLAY_O : DISPLAY_N;
    _alarmDisplay[1] = isAlarmEnabled ? DISPLAY_N : DISPLAY_O;
    _alarmDisplay[2] = tensDigit(toDisplay->hour);
    _alarmDisplay[3] = onesDigit(toDisplay->hour);
    _alarmDisplay[4] = tensDigit(toDisplay->minute);
    _alarmDisplay[5] = onesDigit(toDisplay->minute);

    switch (alarmState.alarmSubMode) {
    case ALARM_VIEW:
        // No blinking in view submode
        _alarmDisplay[7] = 0b000000;
        break;
    default:
        // Blink appropriate digit in other submodes
        _alarmDisplay[7] = 0b000011
                           << (2 * (alarmState.alarmSubMode - ALARM_TOGGLE));
    }
}

// Handle feature specific tasks on each time change from input module
void alarm_onTimeChange(struct Time *time) {
    struct Time *alarmTime = &alarmState.time;

    if (alarmState.isEnabled && time->hour == alarmTime->hour &&
        time->minute == alarmTime->minute && time->second == 0) {
        _prepareBeepingDisplay();
        _startBeep(_beepDisplay, _timeout, _song);
    }
}

static void _onEnterState() { _alarmStateDirty = true; }

// Handle 'Push C' event
static void _onChangePress() {
    _setSubMode(
        boundIncrement(alarmState.alarmSubMode, ALARM_VIEW, ALARM_SET_MINUTE));

    switch (alarmState.alarmSubMode) {
    case ALARM_VIEW:
        _updateAlarm(&alarmState.tmpTime, alarmState.tmpIsEnabled);
        break;

    case ALARM_TOGGLE:
        _updateTmpAlarm(&alarmState.time, alarmState.isEnabled);
        break;

    default:
        // do nothing
        break;
    }
}

// Handle 'Long Push C' event
static void _onChangeLongPress() { _setSubMode(ALARM_VIEW); }

// Handle 'Rotary knob turn' event
static void _onRotaryKnobTurn(int8_t steps) {
    struct Time *time = &alarmState.tmpTime;

    switch (alarmState.alarmSubMode) {
    case ALARM_TOGGLE:
        _updateTmpIsEnabled(!alarmState.tmpIsEnabled);
        break;

    case ALARM_SET_HOUR:
        _updateTmpTimeHour(boundAddition(time->hour, steps, 0, 23));
        break;

    case ALARM_SET_MINUTE:
        _updateTmpTimeMinute(boundAddition(time->minute, steps, 0, 59));
        break;

    default:
        display_changeBrightness(steps);
        break;
    }
}

// Handle a display freshness query
static bool _onQueryDisplayStale() { return _alarmStateDirty; }

// Handle a display hascii query
static void _onQueryHascii(uint8_t **values) {
    _alarmStateDirty = false;
    _recomputeDisplay();
    *values = _alarmDisplay;
}
