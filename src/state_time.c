#include "state_time.h"

#include "display.h"
#include "hascii_constants.h"
#include "state.h"
#include <stdbool.h>

static void _onEnterState();
static void _onChangePress();
static void _onChangeLongPress();
static void _onRotaryKnobTurn(int8_t steps);
static void _onQueryHascii(uint8_t **values);
static bool _onQueryDisplayStale();

static uint8_t _timeDisplay[8];
static uint8_t _timeStateDirty = true;
static void (*_onTimeSet)(struct Time *time);

struct TimeState timeState = {
    .timeSubMode = TIME_VIEW,
    .time =
        {
            .hour = 0,
            .minute = 0,
            .second = 0,
        },
    .handlers =
        {
            .onEnterState = _onEnterState,
            .onChangeLongPress = _onChangeLongPress,
            .onChangePress = _onChangePress,
            .onQueryHascii = _onQueryHascii,
            .onQueryDisplayStale = _onQueryDisplayStale,
            .onRotaryKnobTurn = _onRotaryKnobTurn,
        },
};

// update display variable according to given time struct
static inline void _updateTime(struct Time *time) {
    timeState.time.hour = time->hour;
    timeState.time.minute = time->minute;
    timeState.time.second = time->second;
    _timeStateDirty = true;
}

static inline void _updateTmpTime(struct Time *time) {
    timeState.tmpTime.hour = time->hour;
    timeState.tmpTime.minute = time->minute;
    timeState.tmpTime.second = time->second;
    _timeStateDirty = true;
}

static inline void _updateTmpTimeHour(uint8_t hour) {
    timeState.tmpTime.hour = hour;
    _timeStateDirty = true;
}

static inline void _updateTmpTimeMinute(uint8_t minute) {
    timeState.tmpTime.minute = minute;
    _timeStateDirty = true;
}

static inline void _updateTmpTimeSecond(uint8_t second) {
    timeState.tmpTime.second = second;
    _timeStateDirty = true;
}

static inline void _recomputeDisplay() {
    struct Time *toDisplay = (timeState.timeSubMode == TIME_VIEW)
                                 ? &timeState.time
                                 : &timeState.tmpTime;

    _timeDisplay[0] = tensDigit(toDisplay->hour);
    _timeDisplay[1] = onesDigit(toDisplay->hour);
    _timeDisplay[2] = tensDigit(toDisplay->minute);
    _timeDisplay[3] = onesDigit(toDisplay->minute);
    _timeDisplay[4] = tensDigit(toDisplay->second);
    _timeDisplay[5] = onesDigit(toDisplay->second);

    switch (timeState.timeSubMode) {
    case TIME_VIEW:
        // No blinking on time view submode
        _timeDisplay[7] = 0b000000;
        break;
    default:
        // Blink the appropriate two digits on other submodes
        _timeDisplay[7] = 0b000011
                          << (2 * (timeState.timeSubMode - TIME_SET_HOUR));
    }
}

static inline void _setSubMode(enum TimeSubMode subMode) {
    timeState.timeSubMode = subMode;
    _timeStateDirty = true;
}

static void _onEnterState() { _timeStateDirty = true; }

// Handle 'Push C' event
static void _onChangePress() {
    _setSubMode(
        boundIncrement(timeState.timeSubMode, TIME_VIEW, TIME_SET_SECOND));

    switch (timeState.timeSubMode) {
    case TIME_VIEW:
        _updateTime(&timeState.tmpTime);
        _onTimeSet(&timeState.time);
        break;

    case TIME_SET_HOUR:
        _updateTmpTime(&timeState.time);
        break;

    default:
        // do nothing
        break;
    }
}

// Handle 'Long Push C' event
static void _onChangeLongPress() { _setSubMode(TIME_VIEW); }

// Handle 'Rotary knob turn' event
static void _onRotaryKnobTurn(int8_t steps) {
    struct Time *time = &timeState.tmpTime;

    switch (timeState.timeSubMode) {
    case TIME_SET_HOUR:
        _updateTmpTimeHour(boundAddition(time->hour, steps, 0, 23));
        break;

    case TIME_SET_MINUTE:
        _updateTmpTimeMinute(boundAddition(time->minute, steps, 0, 59));
        break;

    case TIME_SET_SECOND:
        _updateTmpTimeSecond(boundAddition(time->second, steps, 0, 59));
        break;

    default:
        display_changeBrightness(steps);
        break;
    }
}

// Handle a display hascii query
static void _onQueryHascii(uint8_t **values) {
    _timeStateDirty = false;
    _recomputeDisplay();
    *values = _timeDisplay;
}

// Handle a display freshness query
static bool _onQueryDisplayStale() { return _timeStateDirty; }

// Handle feature specific tasks on each time change from input module
void time_onTimeChange(struct Time *time) { _updateTime(time); }

// Set a callback to be called when a time is set by the user
void time_setTimeSetCallback(void (*onTimeSet)(struct Time *time)) {
    _onTimeSet = onTimeSet;
}
