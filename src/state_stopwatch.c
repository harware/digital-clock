#include "state_stopwatch.h"

#include "display.h"
#include "hascii_constants.h"

static void _onEnterState();
static void _onChangePress();
static void _onChangeLongPress();
static void _onRotaryKnobTurn(int8_t steps);
static void _onQueryHascii(uint8_t **values);
static bool _onQueryDisplayStale();

static uint8_t _stopwatchDisplay[8];
static uint8_t _stopwatchStateDirty = true;

struct StopwatchState stopwatchState = {
    .stopwatchSubMode = STOPWATCH_PAUSED,
    .time =
        {
            .hour = 0,
            .minute = 0,
            .second = 0,
        },
    .handlers =
        {
            .onEnterState = _onEnterState,
            .onChangeLongPress = _onChangeLongPress,
            .onChangePress = _onChangePress,
            .onQueryHascii = _onQueryHascii,
            .onQueryDisplayStale = _onQueryDisplayStale,
            .onRotaryKnobTurn = _onRotaryKnobTurn,
        },
};

static inline void _setSubMode(enum StopwatchSubMode subMode) {
    stopwatchState.stopwatchSubMode = subMode;
}

static inline void _updateTime(uint8_t hour, uint8_t minute, uint8_t second) {
    stopwatchState.time.hour = hour;
    stopwatchState.time.minute = minute;
    stopwatchState.time.second = second;
    _stopwatchStateDirty = true;
}

static inline void _recomputeDisplay() {
    struct Time *toDisplay = &stopwatchState.time;

    _stopwatchDisplay[0] = tensDigit(toDisplay->hour);
    _stopwatchDisplay[1] = onesDigit(toDisplay->hour);
    _stopwatchDisplay[2] = tensDigit(toDisplay->minute);
    _stopwatchDisplay[3] = onesDigit(toDisplay->minute);
    _stopwatchDisplay[4] = tensDigit(toDisplay->second);
    _stopwatchDisplay[5] = onesDigit(toDisplay->second);

    switch (stopwatchState.stopwatchSubMode) {
    case STOPWATCH_PAUSED:
        if (stopwatchState.time.hour == 0 && stopwatchState.time.minute == 0 &&
            stopwatchState.time.second == 0) {
            // Do not blink any digit if stopwatch is in the inital (paused
            // on 0) state.
            _stopwatchDisplay[7] = 0b000000;
        } else {
            // Blink all digits if stopwatch is paused in non-initial state
            _stopwatchDisplay[7] = 0b111111;
        }
        break;
    default:
        _stopwatchDisplay[7] = 0b000000;
    }
}

static void _onEnterState() { _stopwatchStateDirty = true; }

// Handle 'Push C' event
static void _onChangePress() {
    _setSubMode(boundIncrement(stopwatchState.stopwatchSubMode,
                               STOPWATCH_PAUSED, STOPWATCH_COUNTING));
}

// Handle 'Long Push C' event
static void _onChangeLongPress() {
    _updateTime(0, 0, 0);
    _setSubMode(STOPWATCH_PAUSED);
}

// Handle 'Rotary knob turn' event
static void _onRotaryKnobTurn(int8_t steps) { display_changeBrightness(steps); }

// Handle a display hascii query
static void _onQueryHascii(uint8_t **values) {
    _stopwatchStateDirty = false;
    _recomputeDisplay();
    *values = _stopwatchDisplay;
}

// Handle a display freshness query
static bool _onQueryDisplayStale() { return _stopwatchStateDirty; }

// Handle feature specific tasks on each time change from input module
void stopwatch_onTimeChange(struct Time *time) {
    if (stopwatchState.stopwatchSubMode == STOPWATCH_COUNTING) {
        struct Time *count = &stopwatchState.time;

        uint8_t hour = count->hour;
        uint8_t minute = count->minute;
        uint8_t second = count->second;

        if (second == 59 && minute == 59) {
            hour = boundAddition(count->hour, 1, 0, 99);
        }

        if (second == 59) {
            minute = boundAddition(count->minute, 1, 0, 59);
        }

        second = boundAddition(count->second, 1, 0, 59);
        _updateTime(hour, minute, second);
    }
}
