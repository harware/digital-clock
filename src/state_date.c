#include "state_date.h"

#include "display.h"
#include "hascii_constants.h"

static void _onEnterState();
static void _onChangePress();
static void _onChangeLongPress();
static void _onRotaryKnobTurn(int8_t steps);
static void _onQueryHascii(uint8_t **values);
static bool _onQueryDisplayStale();

static uint8_t _dateDisplay[8];
static bool _dateStateDirty = true;
static uint8_t _daysLimit = 31;
static uint8_t _daysInMonth[12] = {31, 28, 31, 30, 31, 30,
                                   31, 31, 30, 31, 30, 31};
static void (*_onDateSet)(struct Date *date);

struct DateState dateState = {
    .dateSubMode = DATE_VIEW,
    .date =
        {
            .year = 2000,
            .month = 1,
            .day = 1,
        },
    .handlers =
        {
            .onEnterState = _onEnterState,
            .onChangeLongPress = _onChangeLongPress,
            .onChangePress = _onChangePress,
            .onQueryHascii = _onQueryHascii,
            .onQueryDisplayStale = _onQueryDisplayStale,
            .onRotaryKnobTurn = _onRotaryKnobTurn,
        },
};

// check if the given year is a leap year or not
static bool inline _isLeapYear(uint16_t year) {
    return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

// get the number of days for given year and month combination
static inline int8_t _getDaysLimit(uint16_t year, uint8_t month) {
    if (_isLeapYear(year) && month == 2) {
        return 29;
    }

    return _daysInMonth[month - 1];
}

static inline void _setSubMode(enum DateSubMode subMode) {
    dateState.dateSubMode = subMode;
    _dateStateDirty = true;
}

static inline void _updateDate(struct Date *date) {
    dateState.date.year = date->year;
    dateState.date.month = date->month;
    dateState.date.day = date->day;
    _dateStateDirty = true;
}

static inline void _updateTmpDate(struct Date *date) {
    dateState.tmpDate.year = date->year;
    dateState.tmpDate.month = date->month;
    dateState.tmpDate.day = date->day;
    _dateStateDirty = true;
}

static inline void _updateTmpDateYear(uint16_t year) {
    dateState.tmpDate.year = year;
    _dateStateDirty = true;
}

static inline void _updateTmpDateMonth(uint8_t month) {
    dateState.tmpDate.month = month;
    _dateStateDirty = true;
}

static inline void _updateTmpDateDay(uint8_t day) {
    dateState.tmpDate.day = day;
    _dateStateDirty = true;
}

static inline void _recomputeDisplay() {
    struct Date *toDisplay = (dateState.dateSubMode == DATE_VIEW)
                                 ? &dateState.date
                                 : &dateState.tmpDate;

    if (dateState.dateSubMode == DATE_SET_YEAR) {
        _dateDisplay[0] = thousandsDigit16(toDisplay->year);
        _dateDisplay[1] = hundredsDigit16(toDisplay->year);
        _dateDisplay[2] = tensDigit16(toDisplay->year);
        _dateDisplay[3] = onesDigit16(toDisplay->year);
        _dateDisplay[4] = tensDigit(toDisplay->month);
        _dateDisplay[5] = onesDigit(toDisplay->month);
    } else {
        _dateDisplay[0] = tensDigit16(toDisplay->year);
        _dateDisplay[1] = onesDigit16(toDisplay->year);
        _dateDisplay[2] = tensDigit(toDisplay->month);
        _dateDisplay[3] = onesDigit(toDisplay->month);
        _dateDisplay[4] = tensDigit(toDisplay->day);
        _dateDisplay[5] = onesDigit(toDisplay->day);
    }

    switch (dateState.dateSubMode) {
    case DATE_VIEW:
        // No blinking in view state
        _dateDisplay[7] = 0b000000;
        break;
    case DATE_SET_YEAR:
        // Blink the first four digits in date set state because year has
        // four digits
        _dateDisplay[7] = 0b001111;
        break;
    default:
        // In other submodes, blink the appropriate digits
        _dateDisplay[7] = 0b000011
                          << (2 * (dateState.dateSubMode - DATE_SET_YEAR));
    }
}

// Handle 'Push C' event
static void _onChangePress() {
    _setSubMode(boundIncrement(dateState.dateSubMode, DATE_VIEW, DATE_SET_DAY));

    switch (dateState.dateSubMode) {
    case DATE_VIEW:
        _updateDate(&dateState.tmpDate);
        _onDateSet(&dateState.tmpDate);
        break;

    case DATE_SET_YEAR:
        _updateTmpDate(&dateState.date);
        break;

    case DATE_SET_DAY:
        _daysLimit =
            _getDaysLimit(dateState.tmpDate.year, dateState.tmpDate.month);

        if (dateState.tmpDate.day > _daysLimit) {
            _updateTmpDateDay(_daysLimit);
        }
        break;

    default:
        // do nothing
        break;
    }
}

static void _onEnterState() { _dateStateDirty = true; }

// Handle 'Long Push C' event
static void _onChangeLongPress() { _setSubMode(DATE_VIEW); }

// Handle 'Rotary knob turn' event
static void _onRotaryKnobTurn(int8_t steps) {
    struct Date *date = &dateState.tmpDate;

    switch (dateState.dateSubMode) {
    case DATE_SET_YEAR:
        _updateTmpDateYear(boundAddition16(date->year, steps, 2000, 2099));
        break;

    case DATE_SET_MONTH:
        _updateTmpDateMonth(boundAddition(date->month, steps, 1, 12));
        break;

    case DATE_SET_DAY:
        _updateTmpDateDay(boundAddition(date->day, steps, 1, _daysLimit));
        break;

    default:
        display_changeBrightness(steps);
        break;
    }
}

// Handle a display hascii query
static void _onQueryHascii(uint8_t **values) {
    _dateStateDirty = false;
    _recomputeDisplay();
    *values = _dateDisplay;
}

// Handle a display freshness query
static bool _onQueryDisplayStale() { return _dateStateDirty; }

// Handle feature specific tasks on each date change from input module
void date_onDateChange(struct Date *date) { _updateDate(date); }

// Set a callback to be called when a date is set by the user
void date_setDateSetCallback(void (*onDateSet)(struct Date *date)) {
    _onDateSet = onDateSet;
}
