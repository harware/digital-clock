#include "timekeeper.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/twi.h>

#include "rtc.h"
#include "state.h"

#define SQW_PIN_MASK 0x80
#define SQW_DDR DDRB
#define SQW_PIN PINB
#define SQW_PULLUPS PORTB

// Callbacks to notify changes
static void (*volatile _onTimeChange)(struct Time *time);
static void (*volatile _onDateChange)(struct Date *date);

static struct Time _time;
static struct Date _date;

static volatile bool _timeSetPending = false;
static volatile bool _dateSetPending = false;
static volatile bool _timeAskPending = false;
static volatile bool _dateAskPending = false;
static void _performPendingAction();
static uint8_t _data[3];

static void _processReceivedTime() {
    // BCD byte for seconds
    _time.second = ((0x70 & _data[0]) >> 4) * 10 + (0x0F & _data[0]);
    _dateAskPending = _dateAskPending || _time.second == 59;
    // BCD byte for minutes
    _time.minute = ((0x70 & _data[1]) >> 4) * 10 + (0x0F & _data[1]);
    // BCD byte for hours
    _time.hour = ((0x30 & _data[2]) >> 4) * 10 + (0x0F & _data[2]);
    // Call the callback
    _onTimeChange(&_time);
    // Check and perform a pending action
    _performPendingAction();
}

static void _processReceivedDate() {
    // BCD byte for date (day)
    _date.day = ((0x30 & _data[0]) >> 4) * 10 + (0x0F & _data[0]);
    // BCD byte for month
    _date.month = ((0x10 & _data[1]) >> 4) * 10 + (0x0F & _data[1]);
    // BCD byte for year
    _date.year = 2000 + ((0xF0 & _data[2]) >> 4) * 10 + (0x0F & _data[2]);
    // Call the callback
    _onDateChange(&_date);
    // Check and perform a pending action
    _performPendingAction();
}

static void _askDate() {
    struct DataPacket dataPacket = {
        .address = 0x04, .length = 3, .bytes = _data};
    rtc_getBytes(&dataPacket, _processReceivedDate);
}

static void _askTime() {
    struct DataPacket dataPacket = {
        .address = 0x00, .length = 3, .bytes = _data};
    rtc_getBytes(&dataPacket, _processReceivedTime);
}

static void _setDate() {
    _data[0] = (tensDigit(_date.day) << 4) | onesDigit(_date.day);
    _data[1] = (tensDigit(_date.month) << 4) | onesDigit(_date.month);
    _data[2] = (tensDigit16(_date.year) << 4) | onesDigit16(_date.year);
    struct DataPacket dataPacket = {
        .address = 0x04, .length = 3, .bytes = _data};
    rtc_putBytes(&dataPacket, _performPendingAction);
}

static void _setTime() {
    // And with the 0x7F mask to prevent setting the clock halt
    // bit even if the second takes abnormal values
    _data[0] =
        0x7F & ((tensDigit(_time.second) << 4) | onesDigit(_time.second));
    _data[1] = (tensDigit(_time.minute) << 4) | onesDigit(_time.minute);
    _data[2] = (tensDigit(_time.hour) << 4) | onesDigit(_time.hour);
    struct DataPacket dataPacket = {
        .address = 0x00, .length = 3, .bytes = _data};
    rtc_putBytes(&dataPacket, _performPendingAction);
}

// Enable the square wave output of the RTC module so we can the
// time every second
static void _enableSquarewaveOutput() {
    _data[0] = 0x10;
    struct DataPacket dataPacket = {
        .address = 0x07, .length = 1, .bytes = _data};
    _dateAskPending = true;
    rtc_putBytes(&dataPacket, _askTime);
    // Set the square wave pin to input
    SQW_DDR &= ~SQW_PIN_MASK;
    // Enable the pull-up register (because the square wave output line is
    // open-drain)
    SQW_PULLUPS |= SQW_PIN_MASK;
    // Enable pin change interrupt 0 (PCINT0-PCINT7) (PB0-PB7) in the
    // pin change interrupt control register (PCICR)
    PCICR |= 1 << PCIE0;
    // Enable interrupt only for the pin we have used
    PCMSK0 = SQW_PIN_MASK;
    // Enable interrupts globally
    sei();
}

static void _performPendingAction() {
    if (rtc_isBusy()) {
        return;
    } else if (_timeSetPending) {
        _setTime();
        _timeSetPending = false;
    } else if (_timeAskPending) {
        _askTime();
        _timeAskPending = false;
    } else if (_dateSetPending) {
        _setDate();
        _dateSetPending = false;
    } else if (_dateAskPending) {
        _askDate();
        _dateAskPending = false;
    }
}

static void _timekeeper_onTimeSet(struct Time *time) {
    // Set the time to the obtained time
    _time.second = time->second;
    _time.minute = time->minute;
    _time.hour = time->hour;
    if (rtc_isBusy()) {
        // If the RTC is busy, just set a pending flag.
        _timeSetPending = true;
    } else {
        // Else, set the time
        _setTime();
    }
}

static void _timekeeper_onDateSet(struct Date *date) {
    // Set the date to the obtained date
    _date.day = date->day;
    _date.month = date->month;
    _date.year = date->year;
    if (rtc_isBusy()) {
        // If the RTC is busy, just set a pending flag.
        _dateSetPending = true;
    } else {
        // Else, set the date
        _setDate();
    }
}

// Interrupt service routine for pin change interrupt 0
ISR(PCINT0_vect) { // NOLINT (clang-diagnostic-unknown-attributes)
    // Do work only on rising edge
    if ((SQW_PIN & SQW_PIN_MASK) != 0) {
        if (rtc_isBusy()) {
            // If the RTC is busy, just set a pending flag.
            _timeAskPending = true;
        } else {
            // Else, query the time
            _askTime();
        }
    }
}

void timekeeper_init() {
    // Initialize RTC module
    rtc_init();
    // Register time set listeners with state machine
    state_setTimeSetCallback(_timekeeper_onTimeSet);
    state_setDateSetCallback(_timekeeper_onDateSet);
    // Start the square wave output of the RTC module so we can get time
    // updates every second
    _enableSquarewaveOutput();
}

void timekeeper_setTimeChangeCallback(void (*onTimeChange)(struct Time *time)) {
    _onTimeChange = onTimeChange;
}

void timekeeper_setDateChangeCallback(void (*onDateChange)(struct Date *date)) {
    _onDateChange = onDateChange;
}
