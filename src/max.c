#include "max.h"

inline static void _slaveSelect() { SPI_PORT &= ~(1 << SS_PIN); }

inline static void _slaveDeselect() { SPI_PORT |= (1 << SS_PIN); }

// Initialize the MAX 7219 module
void max_init() {
    // Set slave select line, master out, and clock line as outputs
    SPI_DDR |= (1 << SS_PIN) | (1 << MOSI_PIN) | (1 << SCK_PIN);
    // Start in deselected state
    _slaveDeselect();
    // Set bits in the SPI control register. Not setting the SPRX bits makes
    // the clock divisor be 4, setting MSTR makes SPI work in master mode, and
    // SPE enables SPI.
    SPCR = (1 << MSTR) | (1 << SPE);
    // Wake device from its shutdown mode
    max_writeAddressAndData(SHUTDOWN_REGISTER, AWAKE);
    // Set the IC to scan only seven digits (six 7-segments and mode LEDs)
    max_writeAddressAndData(SCAN_LIMIT_REGISTER, SCAN_SEVEN_DIGITS);
    // Turn off BCD decoding in IC, because we do that on our own
    max_writeAddressAndData(DECODE_MODE_REGISTER, NO_DECODE);
    // Set the display intensity (controlled by the duty cycle) to half
    max_writeAddressAndData(INTENSITY_REGISTER, HALF_INTENSITY);
}

// Write the address and data byte to the MAX 7219 chip, that is, write the
// data byte to the address given by `address`
void max_writeAddressAndData(uint8_t address, uint8_t data) {
    // Send a slave select signal and give byte to SPI hardware
    _slaveSelect();
    SPDR = address;
    // Wait for the write to finish
    loop_until_bit_is_set(SPSR, SPIF);
    // Give data byte and wait again for write to finish
    SPDR = data;
    loop_until_bit_is_set(SPSR, SPIF);
    // Deselect slave. This is necessary for the MAX 7219 IC to retain the
    // written data.
    _slaveDeselect();
}
