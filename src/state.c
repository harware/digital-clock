#include "state.h"

#include "common.h"
#include "display.h"
#include "hascii_constants.h"
#include "input.h"
#include "timekeeper.h"

extern struct AlarmState alarmState;
extern struct BeepState beepState;
extern struct DateState dateState;
extern struct StopwatchState stopwatchState;
extern struct TimeState timeState;
extern struct TimerState timerState;

// variable to store currently visible feature mode and temporary to hold mode
// user is in when Beep is triggered
enum MainMode mainMode = TIME;
enum MainMode beforeBeepMode = TIME;

struct Handlers *_getCurrentModeHandlers() {
    switch (mainMode) {
    case TIME:
        return &timeState.handlers;

    case DATE:
        return &dateState.handlers;

    case ALARM:
        return &alarmState.handlers;

    case STOPWATCH:
        return &stopwatchState.handlers;

    case TIMER:
        return &timerState.handlers;

    case BEEP:
        return &beepState.handlers;

    default:
        // To suppress compiler warning
        return NULL;
    }
}

// start the beeping triggered by the initializer
static void _startBeeping(uint8_t *display, uint8_t timeout, uint8_t song) {
    // while it is already in beep and some other trigger it again, "before beep
    // mode" should not be changed
    if (mainMode != BEEP) {
        beforeBeepMode = mainMode;
        mainMode = BEEP;
    }

    _getCurrentModeHandlers()->onEnterState();
    beep_startBeeping(display, timeout, song);
}

// stop the beeping and reset mode to initializer
static void _stopBeeping() {
    mainMode = beforeBeepMode;
    _getCurrentModeHandlers()->onEnterState();
}

/**
 * Date/Time change handlers
 * These callbacks will be registered with input module and gets called by
 * by the input on every date and/or time changes acknowledged from RTC
 */

// Handle time change interrupts from inpute module
void _state_onTimeChange(struct Time *time) {
    beep_onTimeChange(time);
    time_onTimeChange(time);
    alarm_onTimeChange(time);
    timer_onTimeChange(time);
    stopwatch_onTimeChange(time);
}

// Handle date change interrupts from inpute module
void _state_onDateChange(struct Date *date) { date_onDateChange(date); }

/**
 * Input handlers.
 * These callbacks need to be registered with the input module.
 */

// Handle a 'main mode button press' event
void _onModePress() {
    if (mainMode == BEEP) {
        // This call handles mode changes too
        beep_stopBeeping();
    } else {
        mainMode = boundIncrement(mainMode, TIME, TIMER);
    }
    _getCurrentModeHandlers()->onEnterState();
}

// Handle a 'Push C' event
void _onChangePress() { _getCurrentModeHandlers()->onChangePress(); }

// Handle a 'Long Push C' event
void _onChangeLongPress() { _getCurrentModeHandlers()->onChangeLongPress(); }

// Handle a 'Rotary Knob turn' event
void _onRotaryKnobTurn(int8_t steps) {
    _getCurrentModeHandlers()->onRotaryKnobTurn(steps);
}

// Handle a display hascii query event
static void _onQueryHascii(uint8_t **values) {
    _getCurrentModeHandlers()->onQueryHascii(values);
    (*values)[6] = mainMode;
}

// Handle a display update needed query event
static bool _onQueryDisplayStale() {
    return _getCurrentModeHandlers()->onQueryDisplayStale();
}

// Register date/time keeper callbacks with input module
static void _registerTimekeeperHandlers() {
    timekeeper_setTimeChangeCallback(_state_onTimeChange);
    timekeeper_setDateChangeCallback(_state_onDateChange);
}

// Register input callbacks with input module
static void _registerInputHandlers() {
    input_setModePressCallback(_onModePress);
    input_setChangePressCallback(_onChangePress);
    input_setChangeLongPressCallback(_onChangeLongPress);
    input_setRotaryKnobTurnCallback(_onRotaryKnobTurn);
}

// Register display hascii query callback with display module
static inline void _registerDisplayCallbacks() {
    display_setDisplayHasciiCallback(_onQueryHascii);
    display_setIsDisplayStaleCallback(_onQueryDisplayStale);
}

// Register callbacks specially dedicated with starting and stopping beep
static inline void _registerBeepCallbacks() {
    beep_setStopCallback(_stopBeeping);
    alarm_setBeepStartCallback(_startBeeping);
    timer_setBeepStartCallback(_startBeeping);
}

/**
 * State initializer
 */
void state_init() {
    _registerTimekeeperHandlers();
    _registerInputHandlers();
    _registerDisplayCallbacks();
    _registerBeepCallbacks();
}

/**
 * Register callbacks to state to be notified of time change.
 */
void state_setTimeSetCallback(void (*onTimeSet)(struct Time *time)) {
    time_setTimeSetCallback(onTimeSet);
}

/**
 * Register callbacks to state to be notified of date change.
 */
void state_setDateSetCallback(void (*onDateSet)(struct Date *date)) {
    date_setDateSetCallback(onDateSet);
}
