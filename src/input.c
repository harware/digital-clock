#include "input.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdbool.h>

#define INPUT_DDR DDRC
#define INPUT_PULLUPS PORTC
#define INPUT_PINS PINC
#define INPUT_PINS_MASK 0x0F
#define INPUT_PULLUPS_MASK 0x03
#define INPUT_INTERRUPTS_MASK 0x07

#define MODE_PIN PC0
#define CHANGE_PIN PC1
#define KNOB_A PC2
#define KNOB_B PC3

// Callbacks
static void (*volatile _onModePress)(void);
static void (*volatile _onChangePress)(void);
static void (*volatile _onChangeLongPress)(void);
static void (*volatile _onRotaryKnobTurn)(int8_t);

// Whether or not a change was pressed. We will need to know this to
// detect a short press.
static volatile bool _changedPressed = false;
// Whether or not to ignore the next release of the change button.
// We will need to ignore a release event after a long press is detected.
static volatile bool _ignoreChangeRelease = false;
// We store the previous state of the 'A' output of the rotary encoder because
// when an interrupt fires, we need to know if the output has actually changed
// before we take any action.
static volatile bool _knobAWasHigh = false;

static void _timer_start() {
    // Set timer mode CTC (Clear Timer on Compare match), and comparison
    // source as OCR1A (Output Compare Register 1 A)
    TCCR1B |= 1 << WGM12;
    // Setting the CS12 and CS10 bits the clock divisor to 1024. With our
    // current 1Mhz, the clock tick will be 1.024ms
    TCCR1B |= (1 << CS12) | (1 << CS10);
    // Set 1000 as the value to compare the counter with, because 1000*1.024ms
    // is 1.024s and that's about how long we want the long press to be.
    OCR1A = 1000;
    // Enable interrupt on match with register OCR1A.
    TIMSK1 |= 1 << OCIE1A;
    // Reset counter to zero
    TCNT1 = 0;
    // Enable global interrupts, just to be sure.
    sei();
}

static void _timer_stop() {
    // Reset the bits CS10, CS11 and CS12 on TCCR1B. That stops the timer.
    TCCR1B &= ~((1 << CS10) | (1 << CS11) | (1 << CS12));
}

/*
 * We disable the linter's unknown attributes diagnostic in the interrupt
 * service routine lines because clang doesn't recognize the externally_visible
 * attribute at the moment. See https://bugs.llvm.org/show_bug.cgi?id=16683 and
 * https://github.com/avr-llvm/clang/issues/5
 */

// Interrupt service routine for pin change interrupt 1
ISR(PCINT1_vect) { // NOLINT (clang-diagnostic-unknown-attributes)
    if (bit_is_clear(INPUT_PINS, MODE_PIN)) {
        _onModePress();
    } else if (bit_is_clear(INPUT_PINS, CHANGE_PIN)) {
        // Don't fire a change press event right now, this could be
        // a long press. We start a timer instead.
        _changedPressed = true;
        _ignoreChangeRelease = false;
        _timer_start();
    } else if (_changedPressed && bit_is_set(INPUT_PINS, CHANGE_PIN)) {
        // If change was previously pressed, and now is in release position,
        // this means a release event happened.
        _changedPressed = false;
        if (!_ignoreChangeRelease) {
            // Change was released without the timer completing. This is a
            // normal (short) press.
            _onChangePress();
        }
        _ignoreChangeRelease = false;
    } else if (!_knobAWasHigh && bit_is_set(INPUT_PINS, KNOB_A)) {
        // Knob A output has changed to high after the last interrupt, which
        // means the knob is moving. We test status of output B and send the
        // appropriate callback.
        if (bit_is_set(INPUT_PINS, KNOB_B)) {
            _onRotaryKnobTurn(1);
        } else {
            _onRotaryKnobTurn(-1);
        }
    }
    _knobAWasHigh = bit_is_set(INPUT_PINS, KNOB_A);
}

// Interrupt service routine for timer (to detect long press)
ISR(TIMER1_COMPA_vect) { // NOLINT (clang-diagnostic-unknown-attributes)
    _timer_stop();
    if (_changedPressed) {
        // We waited, and the change button is still pressed. This is
        // definitely a long press!
        _ignoreChangeRelease = true;
        _onChangeLongPress();
    }
    // We don't need to reset _changedPressed, because the input-handling
    // ISR does that.
}

// Initialize the input module
void input_init() {
    // Set data direction as input (this is the default but lets be explicit)
    INPUT_DDR &= ~INPUT_PINS_MASK;
    // Enable pull-up resistors on the input pins
    INPUT_PULLUPS |= INPUT_PULLUPS_MASK;
    // Enable pin change interrupt 1 (PCINT8-PCINT14 = PC0-PC7) in the Pin
    // Change Interrupt Control Register (PCICR)
    PCICR |= 1 << PCIE1;
    // Enable pin change interrupts only for PCINT8-PCINT11 (PC0-PC2) because
    // that's where our inputs are connected
    PCMSK1 |= INPUT_INTERRUPTS_MASK;
    // Enable interrupts globally
    sei();
}

// Register mode butt_on press callback
void input_setModePressCallback(void (*onModePress)()) {
    _onModePress = onModePress;
}

// Register change butt_on press callback
void input_setChangePressCallback(void (*onChangePress)(void)) {
    _onChangePress = onChangePress;
}

// Register change butt_on long press callback
void input_setChangeLongPressCallback(void (*onChangeLongPress)(void)) {
    _onChangeLongPress = onChangeLongPress;
}

// Register rotary knob turn callback
void input_setRotaryKnobTurnCallback(void (*onRotaryKnobTurn)(int8_t steps)) {
    _onRotaryKnobTurn = onRotaryKnobTurn;
}
