#include "display.h"
#include "input.h"
#include "state.h"
#include "timekeeper.h"

int main(void) {
    // initialize modules
    state_init();
    input_init();
    timekeeper_init();
    beep_init();
    display_init();

    // event loop
    while (1) {
        display_update();
    }

    return 0;
}
