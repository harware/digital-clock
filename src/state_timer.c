#include "state_timer.h"

#include "display.h"
#include "hascii_constants.h"
#include "state.h"

static void _onEnterState();
static void _onChangePress();
static void _onChangeLongPress();
static void _onRotaryKnobTurn(int8_t steps);
static void _onQueryHascii(uint8_t **values);
static bool _onQueryDisplayStale();

static uint8_t _song = 0;
static uint8_t _timeout = 30;
static uint8_t _timerDisplay[8];
static bool _timerStateDirty = true;
static uint8_t _beepDisplay[8] = {0, 0, 0, 0, DISPLAY_T, DISPLAY_I, 0};
static void (*_startBeep)(uint8_t *display, uint8_t timeout, uint8_t song);

struct TimerState timerState = {
    .timerSubMode = TIMER_UNSET,
    .time =
        {
            .hour = 0,
            .minute = 0,
            .second = 0,
        },
    .handlers =
        {
            .onEnterState = _onEnterState,
            .onChangeLongPress = _onChangeLongPress,
            .onChangePress = _onChangePress,
            .onQueryHascii = _onQueryHascii,
            .onQueryDisplayStale = _onQueryDisplayStale,
            .onRotaryKnobTurn = _onRotaryKnobTurn,
        },
};

// update display variable according to given time struct
static inline void _recomputeDisplay() {
    if (timerState.timerSubMode == TIMER_UNSET) {
        _timerDisplay[0] = DISPLAY_DASH;
        _timerDisplay[1] = DISPLAY_DASH;
        _timerDisplay[2] = DISPLAY_DASH;
        _timerDisplay[3] = DISPLAY_DASH;
        _timerDisplay[4] = DISPLAY_DASH;
        _timerDisplay[5] = DISPLAY_DASH;
    } else {

        struct Time *toDisplay;

        if (timerState.timerSubMode == TIMER_COUNTING ||
            timerState.timerSubMode == TIMER_PAUSED) {
            toDisplay = &timerState.time;
        } else {
            toDisplay = &timerState.tmpTime;
        }

        _timerDisplay[0] = tensDigit(toDisplay->hour);
        _timerDisplay[1] = onesDigit(toDisplay->hour);
        _timerDisplay[2] = tensDigit(toDisplay->minute);
        _timerDisplay[3] = onesDigit(toDisplay->minute);
        _timerDisplay[4] = tensDigit(toDisplay->second);
        _timerDisplay[5] = onesDigit(toDisplay->second);
    }

    // Set the display-blink bits according to mode
    switch (timerState.timerSubMode) {
    case TIMER_UNSET:
    case TIMER_COUNTING:
        // In the counting and unset states, we do not blink anything
        _timerDisplay[7] = 0b000000;
        break;
    case TIMER_PAUSED:
        // In the and paused state, we blink all digits
        _timerDisplay[7] = 0b111111;
        break;
    default:
        // For other modes, blink the digits being currently set
        _timerDisplay[7] = 0b000011
                           << (2 * (timerState.timerSubMode - TIMER_SET_HOUR));
    }
}

static inline void _setSubMode(enum TimerSubMode subMode) {
    timerState.timerSubMode = subMode;
    _timerStateDirty = true;
}

static inline void _updateTime(struct Time *time) {
    timerState.time.hour = time->hour;
    timerState.time.minute = time->minute;
    timerState.time.second = time->second;
    _timerStateDirty = true;
}

static inline void _updateTmpTime(struct Time *time) {
    timerState.tmpTime.hour = time->hour;
    timerState.tmpTime.minute = time->minute;
    timerState.tmpTime.second = time->second;
    _timerStateDirty = true;
}

static inline void _updateTmpTimeHour(uint8_t hour) {
    timerState.tmpTime.hour = hour;
    _timerStateDirty = true;
}

static inline void _updateTmpTimeMinute(uint8_t minute) {
    timerState.tmpTime.minute = minute;
    _timerStateDirty = true;
}

static inline void _updateTmpTimeSecond(uint8_t second) {
    timerState.tmpTime.second = second;
    _timerStateDirty = true;
}

static void _onEnterState() { _timerStateDirty = true; }

// Handle 'Push C' event
static void _onChangePress() {
    if (timerState.timerSubMode == TIMER_COUNTING) {
        _setSubMode(TIMER_PAUSED);
    } else if (timerState.timerSubMode == TIMER_PAUSED) {
        _setSubMode(TIMER_COUNTING);
    } else {
        _setSubMode(boundIncrement(timerState.timerSubMode, TIMER_UNSET,
                                   TIMER_COUNTING));

        switch (timerState.timerSubMode) {
        case TIMER_COUNTING:
            _updateTime(&timerState.tmpTime);
            break;

        case TIMER_SET_HOUR:
            _updateTmpTime(&timerState.time);
            break;

        default:
            // do nothing
            break;
        }
    }
}

// Handle 'Long Push C' event
static void _onChangeLongPress() {
    struct Time time = {
        .hour = 0,
        .minute = 0,
        .second = 0,
    };
    _setSubMode(TIMER_UNSET);
    _updateTime(&time);
}

// Handle 'Rotary knob turn' event
static void _onRotaryKnobTurn(int8_t steps) {
    struct Time *time = &timerState.tmpTime;

    switch (timerState.timerSubMode) {
    case TIMER_SET_HOUR:
        _updateTmpTimeHour(boundAddition(time->hour, steps, 0, 23));
        break;

    case TIMER_SET_MINUTE:
        _updateTmpTimeMinute(boundAddition(time->minute, steps, 0, 59));
        break;

    case TIMER_SET_SECOND:
        _updateTmpTimeSecond(boundAddition(time->second, steps, 0, 59));
        break;

    default:
        display_changeBrightness(steps);
        break;
    }
}

// Handle a display hascii query
static void _onQueryHascii(uint8_t **values) {
    _timerStateDirty = false;
    _recomputeDisplay();
    *values = _timerDisplay;
}

// Handle a display freshness query
static bool _onQueryDisplayStale() { return _timerStateDirty; }

void timer_setBeepStartCallback(void (*startBeep)(uint8_t *display,
                                                  uint8_t timeout,
                                                  uint8_t song)) {
    _startBeep = *startBeep;
}

// Handle feature specific tasks on each time change from input module
void timer_onTimeChange(struct Time *time) {
    if (timerState.timerSubMode == TIMER_COUNTING) {
        timerState.time.second = boundDecrement(timerState.time.second, 0, 59);

        if (timerState.time.second == 59) {
            timerState.time.minute =
                boundDecrement(timerState.time.minute, 0, 59);
        }

        if (timerState.time.second == 59 && timerState.time.minute == 59) {
            timerState.time.hour = boundDecrement(timerState.time.hour, 0, 23);
        }

        _timerStateDirty = true;

        if (timerState.time.second == 0 && timerState.time.minute == 0 &&
            timerState.time.hour == 0) {
            _startBeep(_beepDisplay, _timeout, _song);
            _setSubMode(TIMER_UNSET);
        }
    }
}
