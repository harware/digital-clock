# Project-specific details
MCU = atmega328p
F_CPU = 1000000UL

# Program locations
CC = avr-gcc
OBJCOPY = avr-objcopy

# Output directory
BINDIR = bin
# Sources directory
SRCDIR = src
# Headers directory
HEADERDIR = include

# Project name
TARGET = clock
# Files
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS=$(addprefix $(BINDIR)/,$(notdir $(SOURCES:.c=.o)))
HEADERS = $(wildcard $(HEADERDIR)/*.h)

# Compilation options
CFLAGS = -Os -g -std=gnu99 -Wall -DF_CPU=$(F_CPU) -Iinclude
TARGET_ARCH = -mmcu=$(MCU)

# Phony targets (targets without a corresponding file)
.PHONY: all build clean

# Main rules
all: build

build: $(BINDIR)/$(TARGET).hex

clean:
	rm -rf bin/ *.elf *.hex *.obj *.o *.d *.eep *.lst *.lss *.sym *.map \
		*~ *.eeprom

# Explicit pattern rules
$(BINDIR)/%.o: $(SRCDIR)/%.c makefile $(HEADERS) | $(BINDIR)
	$(CC) $(CFLAGS) $(TARGET_ARCH) -c -o $@ $<;

$(BINDIR)/$(TARGET).elf: $(OBJECTS)
	$(CC) $(TARGET_ARCH) $^ -o $@

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

$(BINDIR):
	@[ -d $@ ] || mkdir -p $@
