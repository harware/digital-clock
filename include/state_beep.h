#ifndef STATE_BEEP_H
#define STATE_BEEP_H

#include "common.h"

struct BeepState {
    bool isBeeping;
    struct Handlers handlers;
};

// setup beep mode configuration
void beep_init();

// set the callback to call when beep gets timed out or forced closed
void beep_setStopCallback(void *stopCallback);

// start the beep state initialized at given time by given mode
void beep_startBeeping(uint8_t *display, uint8_t timeout, uint8_t song);

// stop the current beeping
void beep_stopBeeping();

// Handle feature specific tasks on each time change from input module
void beep_onTimeChange(struct Time *time);

#endif // STATE_BEEP_H
