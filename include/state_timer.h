#ifndef STATE_TIMER_H
#define STATE_TIMER_H

#include <common.h>

enum TimerSubMode {
    TIMER_UNSET,
    TIMER_SET_HOUR,
    TIMER_SET_MINUTE,
    TIMER_SET_SECOND,
    TIMER_COUNTING,
    TIMER_PAUSED,
};

struct TimerState {
    enum TimerSubMode timerSubMode;
    struct Time time;
    struct Time tmpTime;
    struct Handlers handlers;
};

// set callback to start beep at the timer time
void timer_setBeepStartCallback(void (*startBeep)(uint8_t *display,
                                                  uint8_t timeout,
                                                  uint8_t song));

// Handle feature specific tasks on each time change from input module
void timer_onTimeChange(struct Time *time);

#endif // STATE_TIMER_H
