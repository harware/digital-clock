#ifndef MAX_H
#define MAX_H

#include <avr/io.h>

#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define SS_PIN PB2
#define MOSI_PIN PB3
#define SCK_PIN PB5

#define DECODE_MODE_REGISTER 0x09
#define INTENSITY_REGISTER 0x0A
#define SCAN_LIMIT_REGISTER 0x0B
#define SHUTDOWN_REGISTER 0x0C

#define NO_DECODE 0x00
#define MAX_INTENSITY 0x0F
#define HALF_INTENSITY 0x07
#define SCAN_SEVEN_DIGITS 0x06
#define AWAKE 0x01

// Initialize the MAX 7219 module
void max_init();

// Write the address and data byte to the MAX 7219 chip, that is, write the
// data byte to the address given by `address`
void max_writeAddressAndData(uint8_t address, uint8_t data);

#endif // MAX_H
