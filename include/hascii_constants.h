#ifndef HASCII_CONSTANTS_H
#define HASCII_CONSTANTS_H

// HASCII representation for A, i, L, n, o, t, -, ?
#define DISPLAY_A 10
#define DISPLAY_I 11
#define DISPLAY_L 12
#define DISPLAY_N 13
#define DISPLAY_O 14
#define DISPLAY_T 15
#define DISPLAY_DASH 16
#define DISPLAY_UNKNOWN 17

#endif // HASCII_CONSTANTS_H
