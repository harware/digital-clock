#ifndef COMMON_H
#define COMMON_H

#include <stdbool.h>
#include <stdint.h>

#define NULL 0

/**
 * Define base data structures
 */

struct Date {
    uint16_t year;
    uint8_t month;
    uint8_t day;
};

struct Time {
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
};

/**
 * Define per-state event handler structure
 */
struct Handlers {
    void (*onEnterState)();
    void (*onChangeLongPress)();
    void (*onChangePress)();
    void (*onRotaryKnobTurn)(int8_t steps);
    void (*onQueryHascii)(uint8_t **values);
    bool (*onQueryDisplayStale)();
};

static inline uint8_t min(int8_t a, int8_t b) { return (a < b) ? a : b; }
static inline uint8_t max(int8_t a, int8_t b) { return (a > b) ? a : b; }

// 8-bit integer manipulations
static inline uint8_t modulo(int8_t a, int8_t b) {
    int8_t m = a % b;

    if (m >= 0) {
        return m;
    }

    return (b < 0) ? m - b : m + b;
}

static inline uint8_t onesDigit(uint8_t x) { return x % 10; }

static inline uint8_t tensDigit(uint8_t x) { return (x / 10) % 10; }

static inline uint8_t hundredsDigit(uint8_t x) { return x / 100; }

static inline uint8_t boundAddition(int8_t value, int8_t i, uint8_t min,
                                    uint8_t max) {
    return min + modulo(value + i - min, max - min + 1);
}

static inline uint8_t boundIncrement(int8_t value, uint8_t min, uint8_t max) {
    return boundAddition(value, 1, min, max);
}

static inline uint8_t boundDecrement(int8_t value, uint8_t min, uint8_t max) {
    return boundAddition(value, -1, min, max);
}

// 16-bit integer manipulations
static inline uint16_t modulo16(int16_t a, int16_t b) {
    int16_t m = a % b;

    if (m >= 0) {
        return m;
    }

    return (b < 0) ? m - b : m + b;
}

static inline uint8_t onesDigit16(uint16_t x) { return x % 10; }

static inline uint8_t tensDigit16(uint16_t x) { return (x / 10) % 10; }

static inline uint8_t hundredsDigit16(uint16_t x) { return (x / 100) % 10; }

static inline uint8_t thousandsDigit16(uint16_t x) { return (x / 1000) % 10; }

static inline uint16_t boundAddition16(int16_t value, int16_t i, uint16_t min,
                                       uint16_t max) {
    return min + modulo16(value + i - min, max - min + 1);
}

#endif // COMMON_H
