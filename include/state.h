#ifndef STATE_H
#define STATE_H

#include "common.h"
#include "state_alarm.h"
#include "state_beep.h"
#include "state_date.h"
#include "state_stopwatch.h"
#include "state_time.h"
#include "state_timer.h"

/**
 * Define various main-modes for each feature of the clock
 */

enum MainMode { TIME, DATE, ALARM, STOPWATCH, TIMER, BEEP };

/**
 * State initializer
 */

void state_init();

/**
 * Register callbacks to state to be notified of time change.
 */
void state_setTimeSetCallback(void (*onTimeSet)(struct Time *time));

/**
 * Register callbacks to state to be notified of date change.
 */
void state_setDateSetCallback(void (*onDateSet)(struct Date *date));

#endif // DISPLAY_H
