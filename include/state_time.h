#ifndef STATE_TIME_H
#define STATE_TIME_H

#include <common.h>

enum TimeSubMode {
    TIME_VIEW,
    TIME_SET_HOUR,
    TIME_SET_MINUTE,
    TIME_SET_SECOND,
};

struct TimeState {
    enum TimeSubMode timeSubMode;
    struct Time time;
    struct Time tmpTime;
    struct Handlers handlers;
};

// Handle feature specific tasks on each time change from input module
void time_onTimeChange(struct Time *time);

// Set a callback to be called when a time is set by the user
void time_setTimeSetCallback(void (*onTimeSet)(struct Time *time));

#endif // STATE_TIME_H
