#ifndef TIMEKEEPER_H
#define TIMEKEEPER_H

#include "common.h"

void timekeeper_init();

void timekeeper_setTimeChangeCallback(void (*onTimeChange)(struct Time *time));

void timekeeper_setDateChangeCallback(void (*onDateChange)(struct Date *date));

#endif // TIMEKEEPER_H
