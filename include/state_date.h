#ifndef STATE_DATE_H
#define STATE_DATE_H

#include <common.h>

enum DateSubMode {
    DATE_VIEW,
    DATE_SET_YEAR,
    DATE_SET_MONTH,
    DATE_SET_DAY,
};

struct DateState {
    enum DateSubMode dateSubMode;
    struct Date date;
    struct Date tmpDate;
    struct Handlers handlers;
};

// Handle feature specific tasks on each date change from input module
void date_onDateChange(struct Date *date);

// Set a callback to be called when a date is set by the user
void date_setDateSetCallback(void (*onDateSet)(struct Date *date));

#endif // STATE_DATE_H
