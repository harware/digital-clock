#ifndef DISPLAY_H
#define DISPLAY_H

#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>

#include "common.h"
#include "state.h"

// Number of digits in the display
#define DIGITS_COUNT 6

// initialize display module
void display_init();

// Run the main display task
void display_update();

// set callback to query whether display needs to be updated
void display_setIsDisplayStaleCallback(bool (*isDisplayStale)());

// change brightness for the seven segment display with provided steps
void display_changeBrightness(int8_t steps);

// set callback to get set of display Hascii
void display_setDisplayHasciiCallback(
    void (*setDisplayHascii)(uint8_t **values));

#endif // DISPLAY_H
