#ifndef INPUT_H
#define INPUT_H

#include <stdint.h>

// Initialize the input module
void input_init();

// Register mode button press callback
void input_setModePressCallback(void (*onModePress)());

// Register change button press callback
void input_setChangePressCallback(void (*onChangePress)(void));

// Register change button long press callback
void input_setChangeLongPressCallback(void (*onChangeLongPress)(void));

// Register rotary knob turn callback
void input_setRotaryKnobTurnCallback(void (*onRotaryKnobTurn)(int8_t steps));

#endif // INPUT_H
