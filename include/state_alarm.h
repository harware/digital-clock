#ifndef STATE_ALARM_H
#define STATE_ALARM_H

#include "common.h"

enum AlarmSubMode {
    ALARM_VIEW,
    ALARM_TOGGLE,
    ALARM_SET_HOUR,
    ALARM_SET_MINUTE,
};

struct AlarmState {
    enum AlarmSubMode alarmSubMode;
    bool isEnabled;
    bool tmpIsEnabled;
    struct Time time;
    struct Time tmpTime;
    struct Handlers handlers;
};

// set callback to start beep at the alarm time
void alarm_setBeepStartCallback(void (*startBeep)(uint8_t *display,
                                                  uint8_t timeout,
                                                  uint8_t song));

// Handle feature specific tasks on each time change from input module
void alarm_onTimeChange(struct Time *time);

#endif // STATE_ALARM_H
