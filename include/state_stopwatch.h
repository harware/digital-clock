#ifndef STATE_STOPWATCH_H
#define STATE_STOPWATCH_H

#include <common.h>

enum StopwatchSubMode {
    STOPWATCH_PAUSED,
    STOPWATCH_COUNTING,
};

struct StopwatchState {
    enum StopwatchSubMode stopwatchSubMode;
    struct Time time;
    struct Handlers handlers;
};

// Handle feature specific tasks on each time change from input module
void stopwatch_onTimeChange(struct Time *time);

#endif // STATE_STOPWATCH_H
