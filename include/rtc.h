#ifndef RTC_H
#define RTC_H

#include <stdbool.h>
#include <stdint.h>

struct DataPacket {
    uint8_t address;
    uint8_t length;
    uint8_t *bytes;
};

/**
 * Initialize the Real Time Clock
 */
void rtc_init();

/**
 * Write 'length' number of bytes from 'bytes' to the DS1307's battery-backed
 * RAM, starting at 'address'. The callback onDataWrite is called after
 * writing finishes.
 */
bool rtc_putBytes(struct DataPacket *dataPacket, void (*onDataWrite)());

/**
 * Read 'length' number of bytes to 'bytes' from the DS1307's battery-backed
 * RAM, starting at 'address'. The callback onDataRead is called when the
 * writing finishes.
 */
bool rtc_getBytes(struct DataPacket *dataPacket, void (*onDataRead)());

/**
 * Return true if the RTC communication module is busy communicating with
 * the RTC. The rtc_getBytes and rtc_putBytes operations will fail (return
 * false) if they are called when the RTC module is busy.
 */
bool rtc_isBusy();

#endif // RTC_H
