# Digital Clock

This is the source repository for a DIY digital clock project built with the
ATMega328P microcontroller. It uses seven-segment displays for displaying the
time and uses a DS1307 real time clock IC for timekeeping.

*Digital Clock* supports the following modes:

1. Time
2. Date
3. Alarm
4. Countdown timer
5. Stopwatch

### Prerequisites
To install the toolchain for building and flashing AVR code, do the following:

#### On Arch Linux
```bash
$ sudo pacman --sync avr-gcc avr-libc avr-binutils avrdude
```

#### On Debian and derivatives (Ubuntu, Mint, etc.)
```bash
$ sudo apt install gcc-avr avr-libc binutils-avr avrdude
```

### Building
To build the source files, run `make` in the project directory:

```bash
$ make
```

Running make will build the object files, the ELF-format executable and the
Intel HEX format file in the `bin/` directory. You can use these files to
either test the code in a simulator or to flash a microcontroller.

To clean the built files, do

```bash
$ make clean
```

### Linting with Vim and Ale

#### Install dependencies
```bash
pacman -S clang cppcheck
```

#### Update linters and fixers in vim config
```
let g:ale_linters = {
  \ 'c': ['cppcheck', 'clangtidy'],
  \ 'cpp': ['cppcheck', 'clangtidy'],
  \ }

let g:ale_fixers = {
  \ 'c': ['clang-format'],
  \ 'cpp': ['clang-format'],
  \ }
```

> The linting of clangtidy is updated only after file is saved and ALELint is called manually.
